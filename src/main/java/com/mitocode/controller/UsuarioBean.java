package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Rol;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable{
	@Inject
	private IUsuarioService usuarioService;
	private Usuario usuario;
	private List<Usuario> lista;
	private String tipoDialog;
	private String user;
	private boolean verificar;
	private String password;
	
	@PostConstruct
	public void init(){
		this.usuario=new Usuario();
		this.lista=new ArrayList<>();
		this.listar();
		this.verificar();	
	}
	
	public void listar() {
		try {
			this.lista = this.usuarioService.listar();
		} catch (Exception e) {

		}
	}
	
	public void listarPorNombre() {
		try {
			this.lista = this.usuarioService.listarPorNombre(user);
		} catch (Exception e) {

		}
	}
	
	public boolean verificar() {
		this.verificar=false;
		try {
			Usuario us = this.usuarioService.login(this.usuario);

			if (us != null && us.getEstado().equalsIgnoreCase("A")) {
				this.verificar = true;
			} else {
				this.verificar=false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.verificar;
	}
	
	public String modificar() {
		String redireccion = null;
		try {
			String clave = this.password;
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setContrasena(claveHash);
			this.usuarioService.modificar(this.usuario);
			redireccion = "/protegido/usuarios?faces-redirect=true";
		}catch(Exception e) {
			
		}
		return redireccion;
	}
	
	
	public boolean isVerificar() {
		return verificar;
	}

	public void setVerificar(boolean verificar) {
		this.verificar = verificar;
	}	

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Usuario> getLista() {
		return lista;
	}
	
	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}
	
	public String getTipoDialog() {
		return tipoDialog;
	}
	
	public void setTipoDialog(String tipoDialog) {
		this.tipoDialog = tipoDialog;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public void mostrarData(Usuario u) {
		this.usuario = u;
		this.tipoDialog = "Modificando Usuario:";
		this.verificar=false;
		
	}
	
	public String cancelar() {
		return "/protegido/usuarios?faces-redirect=true";
	}

	


}
